<?php require_once "./code.php";

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S1: A1</title>
</head>
<body>
	
	<h1>Full Address</h1>
	<p>Full Address: <?= getFullAddress("Medellin", "Cebu", "Philippines"); ?></p>
	<p>Full Address: <?= getFullAddress("Consolacion", "Cebu", "Philippines"); ?></p>

	<h1>Letter-Based Grading</h1>
	<p>87 is equivalent to <?= getLetterGrade(87); ?></p>
	<p>94 is equivalent to <?= getLetterGrade(94); ?></p>
	<p>71 is equivalent to <?= getLetterGrade(71); ?></p>
</body>
</html>