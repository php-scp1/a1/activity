<?php ?>

<?php

// You can omit the closing php tag, but all your code fromt he opening and beyond will be in PHP

// [SECTION] Variables
// Used to contain data
// Named location for a stored value

$name = 'John Smith';
$email = 'johnsmith@gmail.com';

// Variable reassignment
$name = 'Jane Doe';

// echo is used to print value

// echo $name;

//[Section] Constant
// Constant are used to hold data that is meant to be read-only and cannot be changed
define('PI', 3.1416);

/*
$PI = 200;

echo $PI; // for variable PI

echo PI; // for const PI

*/

//[Section] Data types

// Strings
$state = 'New York';
$country = 'United States';

//Integers
$age = 31;
$headcount = 26;

//Floats:
$grade = 98.2;
$distance = 1342.12;

// Boolean
$hasTraveledAbroad = false;
$hasSymptoms = true;

//Null
$spouse = null;
$middleName = null;

//Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);
$students = ["John", "Jane", "Jacob"];

//Objects
$gradesObj = (object)[
'firstGrading' => 98.7,
'secondGrading' => 92.1,
'thirdGrading' => 90.2,
'fourthGrading' => 94.6
];

$personObj = (object)[
'fullName' => 'John Smith',
'isMarried' => false,
'age' => 35,
'address' => (object)[
	'state' => 'New York',
	'country' => 'United States'
	]
];

//[Section] Operators
// Assignment operator
//Used to assign values to variables
$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

//[Section] Functions
//Functions are used to make reusable code
function getFullName($firstName, $middleInitial, $lastName){
	return "$lastName, $firstName $middleInitial";
}

//[Section] Selection Control Structures
// Selection control structures are used to make code execution dynamic according to predefined conditions
function determineTyphoonIntensity($windSpeed){
	if($windSpeed < 30){
		return "Not a typhoon yet";
	}else if($windSpeed <= 61){
		return "Tropical depression detected";
	}
	else if($windSpeed >= 62 && $windSpeed <= 88){
		return "Tropical storm detected";
	}
	else if($windSpeed >= 89 && $windSpeed <= 117){
		return "Sever Tropical storm detected";
	}else{
		return 'Typhoon detected';
	}
}