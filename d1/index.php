<?php require_once "./code.php";
//PHP code can be included from another file by using the require_once directive.
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S1: PHP Basics and Selction Control Structures</title>
</head>
<body>
	<h1>Hello World</h1>
	<p><?php echo 'Good day $name! Your given email is $email.' ?></p>

	<!-- Using double quotes will print values with echo in the middle of the string. -->
	<p><?php echo "Good day $name! Your given email is $email." ?></p>

	<!-- Concatenation (Dots are used instead of lus signs) -->
	<p><?php echo 'Good day' . $name . '!' . 'Your given email is ' . $email . '.';?></p>
	<p><?php echo PI; ?></p> <!-- Still working w/o semicolon -->
	<h1>Data Types</h1>
	<!-- Normal echoing of boolean and null varaibles in PHP will not show the values -->
	<p><?php echo $hasTaveledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>
	<!-- To see their types instead, we can use gettype() -->
	<p><?php echo gettype($spouse); ?></p>

	<!-- To see more deatailed information on the variable, use var_dump -->
	<p><?php echo var_dump($hasTraveledAbroad); ?></p>

	<!-- To output an array element, we can use the usual square brackets -->
	<p><?php echo $grades[3]; ?></p>
	<p><?php echo $students[2]; ?></p>

	<!-- To output an entire array, we can use print_r() -->
	<pre><?php echo print_r($students); ?></pre>

	<!-- To output the value of an object property, the arrow notation can be used -->
	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<?php // "<?=" is shorthand for an opening PHP tag + echo ?>
	<h1>Operators</h1>
	<p><?= "X is $x"; ?></p>
	<p><?= "Y is $x"; ?></p>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?= $x + $y; ?></p>
	<p>Difference: <?= $x - $y; ?></p>
	<p>Product: <?= $x * $y; ?></p>
	<p>Quotient: <?= $x / $y; ?></p>

	<h2>Equality Operators</h2>
	<p>Loose Equality: <?= var_dump($x == 1342.14); ?></p>
	<p>Strict Equality: <?= var_dump($x === 1342.14); ?></p>
	<p>Loose Inequality: <?= var_dump($x != 1342.14); ?></p>
	<p>Strict Inequality: <?= var_dump($x !== 1342.14); ?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is Lesser: <?= var_dump($x < $y); ?></p>
	<p>Is Greater: <?= var_dump($x > $y); ?></p>
	<p>Is Lesser or Equal: <?= var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?= var_dump($x >= $y); ?></p>

	<h2>Logical Operators</h2>
	<p>Is Legal Age: <?= var_dump($isLegalAge); ?></p>
	<p>Is Registered: <?= var_dump($isRegistered); ?></p>

	<p>Are All Requirements Met: <?= var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements Met: <?= var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are Some Requirements Not Met: <?= var_dump(!$isLegalAge && !$isRegistered); ?></p>

	<h1>Functions</h1>
	<p>Full Name: <?= getFullName("John", "B", "Smith"); ?></p>

	<h1>Selction Control Structures</h1>
	<p><?= determineTyphoonIntensity(12); ?></p>
</body>
</html>